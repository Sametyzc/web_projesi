﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dil {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Web_proje.Resource", typeof(Resource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Başlık.
        /// </summary>
        public static string Başlık {
            get {
                return ResourceManager.GetString("Başlık", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilgi.
        /// </summary>
        public static string Bilgi {
            get {
                return ResourceManager.GetString("Bilgi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış.
        /// </summary>
        public static string Cikis {
            get {
                return ResourceManager.GetString("Cikis", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış.
        /// </summary>
        public static string Çıkış {
            get {
                return ResourceManager.GetString("Çıkış", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detaylar.
        /// </summary>
        public static string Detaylar {
            get {
                return ResourceManager.GetString("Detaylar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Düzenle.
        /// </summary>
        public static string Duzenle {
            get {
                return ResourceManager.GetString("Duzenle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Geri Dön.
        /// </summary>
        public static string GeriDon {
            get {
                return ResourceManager.GetString("GeriDon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gezi.
        /// </summary>
        public static string Gezi {
            get {
                return ResourceManager.GetString("Gezi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Geziler.
        /// </summary>
        public static string Geziler {
            get {
                return ResourceManager.GetString("Geziler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gezi Türü.
        /// </summary>
        public static string GeziTuru {
            get {
                return ResourceManager.GetString("GeziTuru", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş.
        /// </summary>
        public static string Giriş {
            get {
                return ResourceManager.GetString("Giriş", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Haber.
        /// </summary>
        public static string Haber {
            get {
                return ResourceManager.GetString("Haber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Haberler.
        /// </summary>
        public static string Haberler {
            get {
                return ResourceManager.GetString("Haberler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkında.
        /// </summary>
        public static string Hakkında {
            get {
                return ResourceManager.GetString("Hakkında", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hata.
        /// </summary>
        public static string Hata {
            get {
                return ResourceManager.GetString("Hata", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İncele.
        /// </summary>
        public static string Incele {
            get {
                return ResourceManager.GetString("Incele", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Katıl.
        /// </summary>
        public static string Katil {
            get {
                return ResourceManager.GetString("Katil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Katılım.
        /// </summary>
        public static string Katilim {
            get {
                return ResourceManager.GetString("Katilim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bu Hesap Kilitli.
        /// </summary>
        public static string Kilitli {
            get {
                return ResourceManager.GetString("Kilitli", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bu hesap kilitlenmiş , daha sonra tekrar deneyin.
        /// </summary>
        public static string Kilitli2 {
            get {
                return ResourceManager.GetString("Kilitli2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı.
        /// </summary>
        public static string Kullanici {
            get {
                return ResourceManager.GetString("Kullanici", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı Adı.
        /// </summary>
        public static string KullaniciAdi {
            get {
                return ResourceManager.GetString("KullaniciAdi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcılar.
        /// </summary>
        public static string Kullanicilar {
            get {
                return ResourceManager.GetString("Kullanicilar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcısının Rolleri.
        /// </summary>
        public static string KullanicininRolleri {
            get {
                return ResourceManager.GetString("KullanicininRolleri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcının Rolleri.
        /// </summary>
        public static string KullanicininRolleri2 {
            get {
                return ResourceManager.GetString("KullanicininRolleri2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcıya Rol Ekle.
        /// </summary>
        public static string KullaniciyaRolEkle {
            get {
                return ResourceManager.GetString("KullaniciyaRolEkle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Listeye Geri Dön.
        /// </summary>
        public static string ListeyeGeriDon {
            get {
                return ResourceManager.GetString("ListeyeGeriDon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Merhaba.
        /// </summary>
        public static string Merhaba {
            get {
                return ResourceManager.GetString("Merhaba", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Önceki.
        /// </summary>
        public static string Onceki {
            get {
                return ResourceManager.GetString("Onceki", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resim.
        /// </summary>
        public static string Resim {
            get {
                return ResourceManager.GetString("Resim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol.
        /// </summary>
        public static string Rol {
            get {
                return ResourceManager.GetString("Rol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Adı.
        /// </summary>
        public static string RolAdi {
            get {
                return ResourceManager.GetString("RolAdi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Ata.
        /// </summary>
        public static string RolAta {
            get {
                return ResourceManager.GetString("RolAta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Düzenle.
        /// </summary>
        public static string RolDuzenle {
            get {
                return ResourceManager.GetString("RolDuzenle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Ekle.
        /// </summary>
        public static string RolEkle {
            get {
                return ResourceManager.GetString("RolEkle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol listesine dön.
        /// </summary>
        public static string RolListesineDon {
            get {
                return ResourceManager.GetString("RolListesineDon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rolü Sil.
        /// </summary>
        public static string RoluSil {
            get {
                return ResourceManager.GetString("RoluSil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sil.
        /// </summary>
        public static string Sil {
            get {
                return ResourceManager.GetString("Sil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Silmek istediğinize emin misiniz ?.
        /// </summary>
        public static string SilEmin {
            get {
                return ResourceManager.GetString("SilEmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gezi, tur düzenle ve katıl.
        /// </summary>
        public static string SiteBaslik {
            get {
                return ResourceManager.GetString("SiteBaslik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sonraki.
        /// </summary>
        public static string Sonraki {
            get {
                return ResourceManager.GetString("Sonraki", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarih.
        /// </summary>
        public static string Tarih {
            get {
                return ResourceManager.GetString("Tarih", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş için başka bir servis kullanın.
        /// </summary>
        public static string UseAnother {
            get {
                return ResourceManager.GetString("UseAnother", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üye Ol.
        /// </summary>
        public static string ÜyeOl {
            get {
                return ResourceManager.GetString("ÜyeOl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Web Sitesi.
        /// </summary>
        public static string WebSitesi {
            get {
                return ResourceManager.GetString("WebSitesi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En Yeni Geziler.
        /// </summary>
        public static string YeniGeziler {
            get {
                return ResourceManager.GetString("YeniGeziler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Haber.
        /// </summary>
        public static string YeniHaber {
            get {
                return ResourceManager.GetString("YeniHaber", resourceCulture);
            }
        }
    }
}
