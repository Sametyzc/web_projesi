﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web_proje.DataAccessLayer;
using Web_proje.Models;



namespace Web_proje.Controllers
{
    public class HomeController : Controller
    {
        
        //Bu sayfaya sadece üye girşi yapanlara gösterilir
        public ActionResult Index()
        {
            /*RolePrincipal r = (RolePrincipal)User;
            var Roller = r.GetRoles();
            Roles.GetRolesForUser();*/
            using (ModelContext context = new ModelContext())//Kullanmak için yeni bir context nesnesi oluşturuldu
            {
                Liste anaSayfa = new Liste();
                anaSayfa.HaberBilgi/*Haberler listesine eşitlendi liste aşağıda */ = context.Habers.OrderByDescending(x => x.Baslik/*Haber modelinin içindeki bilgi ye göre sıralandı*/).Take(5)/*İçine kaç yazarsak o kadar gösterir*/.ToList();
                anaSayfa.Geziler/*Haberler listesine eşitlendi liste aşağıda */ = context.Travels.OrderByDescending(x => x.TravelID).Take(4)/*İçine kaç yazarsak o kadar gösterir*/.ToList();
                return View(anaSayfa);
            }
        }
    }
    public class Liste//
    {
        public List<Haber> HaberBilgi { get; set; }
        public List<Travel> Geziler { get; set; }
    }

}   