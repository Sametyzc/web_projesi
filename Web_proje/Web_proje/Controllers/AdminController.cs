﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_proje.DataAccessLayer;
using Web_proje.Models;

namespace Web_proje.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private ModelContext db = new ModelContext();

        public ActionResult TravelIndex()
        {
            return View(db.Travels.ToList());
        }
        // GET: Travel/Create
        public ActionResult TravelCreate()
        {
            return View();
        }

        // POST: Travel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TravelCreate([Bind(Include = "TravelID,Day,Price,Quota,travelType,Description")] Travel travel)
        {
            if (ModelState.IsValid)
            {
                db.Travels.Add(travel);
                db.SaveChanges();
                return RedirectToAction("TravelIndex");
            }
                
            return View(travel);
        }

        // GET: Travel/Edit/5
        public ActionResult TravelEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // POST: Travel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TravelEdit([Bind(Include = "TravelID,Day,Price,Quota,travelType,Description")] Travel travel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("TravelIndex");
            }
            return View(travel);
        }

        // GET: Travel/Delete/5
        public ActionResult TravelDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }

        // POST: Travel/Delete/5
        [HttpPost, ActionName("TravelDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult TravelDeleteConfirmed(int id)
        {
            Travel travel = db.Travels.Find(id);
            db.Travels.Remove(travel);
            db.SaveChanges();
            return RedirectToAction("TravelIndex");
        }
        public ActionResult HaberIndex()
        {
            return View(db.Habers.ToList());
        }
        public ActionResult HaberCreate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult HaberCreate(Haber d, HttpPostedFileBase file)
        {//Fotoğraf eklemek için Üstteki gibi yazılır HttpPosted olan
            try
            {
                if (ModelState.IsValid)
                {
                    using (ModelContext context = new ModelContext())
                    {

                        Haber _haber = new Haber();
                        if (file != null && file.ContentLength > 0)
                        {
                            MemoryStream memoryStream = file.InputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                file.InputStream.CopyTo(memoryStream);
                            }
                            _haber.Foto = memoryStream.ToArray();
                        }
                        _haber.HaberID = d.HaberID;
                        _haber.Baslik = d.Baslik;

                        _haber.Tarih = d.Tarih;

                        _haber.Bilgi = d.Bilgi;
                        context.Habers.Add(_haber);
                        context.SaveChanges();
                        return RedirectToAction("HaberIndex");
                    }
                }
                else
                {
                    ViewBag.Error = "Eklerken hata oluştu";
                    return View();
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        public ActionResult HaberEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Habers.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HaberEdit(Haber haber, HttpPostedFileBase file)
        {


            using (ModelContext context = new ModelContext())
            {
                var _slideDuzenle = context.Habers.Where(x => x.HaberID == haber.HaberID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slideDuzenle.Foto = memoryStream.ToArray();
                }
                _slideDuzenle.Baslik = haber.Baslik;
                _slideDuzenle.Bilgi = haber.Bilgi;
                _slideDuzenle.Tarih = haber.Tarih;
                context.SaveChanges();
                return RedirectToAction("HaberIndex", "Admin");
            }

        }
        public ActionResult HaberDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Habers.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }
        [HttpPost, ActionName("HaberDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult HaberDeleteConfirmed(int id)
        {
            Haber haber = db.Habers.Find(id);
            db.Habers.Remove(haber);
            db.SaveChanges();
            return RedirectToAction("HaberIndex");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}