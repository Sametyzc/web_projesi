﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_proje.DataAccessLayer;
using Web_proje.Models;

namespace Web_proje.Controllers
{
    public class TravelController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Travel
        public ActionResult Index(int? sonuc)
        {
            if (sonuc == 0)
            {
                ViewBag.Sonuc = "Kayıt başarısız";
            }
            else if(sonuc ==1)
            {
                ViewBag.Sonuc = "Kayıt başarılı";
            }
            else
            {
            }
            return View(db.Travels.ToList());
        }

        // GET: Travel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }
            return View(travel);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
