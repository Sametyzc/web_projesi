﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_proje.DataAccessLayer;
using Web_proje.Models;

namespace Web_proje.Controllers
{
   
    public class HaberController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Haber
        public ActionResult Index()
        {
            return View(db.Habers.ToList());
        }

        // GET: Haber/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Haber haber = db.Habers.Find(id);
            if (haber == null)
            {
                return HttpNotFound();
            }
            return View(haber);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
