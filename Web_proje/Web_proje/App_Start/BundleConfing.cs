﻿using System.Web;
using System.Web.Optimization;

namespace WebProje3
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts").Include(
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/jquery-3.0.0.js",
                     "~/Scripts/popper-utils.js",
                     "~/Scripts/Site.js"    
                     ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/Site.css",
                      "~/Content/css/font-awesome.css"
                      ));
        }
    }
}
