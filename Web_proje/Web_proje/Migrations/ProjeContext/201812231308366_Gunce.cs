namespace Web_proje.Migrations.ProjeContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Gunce : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Registration", "travel_TravelID", "dbo.Travel");
            DropIndex("dbo.Registration", new[] { "travel_TravelID" });
            DropTable("dbo.Registration");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Registration",
                c => new
                    {
                        RegistrationID = c.Int(nullable: false, identity: true),
                        CustomerID = c.Int(nullable: false),
                        travel_TravelID = c.Int(),
                    })
                .PrimaryKey(t => t.RegistrationID);
            
            CreateIndex("dbo.Registration", "travel_TravelID");
            AddForeignKey("dbo.Registration", "travel_TravelID", "dbo.Travel", "TravelID");
        }
    }
}
