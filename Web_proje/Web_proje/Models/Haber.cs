﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_proje.Models
{
    public class Haber
    {
        public int HaberID { get; set; }

        [Required(ErrorMessage = "Alan boş geçilemez")]
        [MinLength(2, ErrorMessage = "Minimum 2 karakterlik giriş yapmalısınız")]
        public string Baslik { get; set; }


        public DateTime Tarih { get; set; }

        [Required(ErrorMessage = "Alan boş geçilemez")]
        [MinLength(10, ErrorMessage = "Minimum 10 karakterlik giriş yapmalısınız")]
        public string Bilgi { get; set; }
        public byte[] Foto { get; set; }/*Fotoğraf eklemek için byte kullanılır*/
}
}