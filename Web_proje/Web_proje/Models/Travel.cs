﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_proje.Models
{
    public enum TravelType
    {
        Trip, Abroad, Domestic, Travel, Festival
    }
    public class Travel
    {
        public int TravelID { get; set; }
        public DateTime Day { get; set; }
     
        public int Price { get; set; }
        public int Quota { get; set; }
        public TravelType travelType { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}