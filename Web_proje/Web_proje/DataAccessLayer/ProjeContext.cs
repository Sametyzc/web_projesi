﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Web_proje.Models;

namespace Web_proje.DataAccessLayer
{
    public class ModelContext : DbContext
    {
        //data olusturulduguda context isimli olusacak
        public ModelContext() : base("ProjeModel")
        {
            Database.SetInitializer<ModelContext>(new DropCreateDatabaseIfModelChanges<ModelContext>()); //modelde degisijlik olursa db yeniden olustugunda veri kaybi yasamamak icin yapici metoda bu kod yazildi
        }

        //tanımlanan siniflar db ye tanitildi

        public DbSet<Travel> Travels { get; set; }
        public DbSet<Haber> Habers { get; set; }

        //db de tablo olusurken s takisi eklememesi icin yazildi
        protected override void OnModelCreating(DbModelBuilder modelBuilder)  
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
    